<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Community-network-email-service](#community-network-email-service)
  - [Idea](#idea)
  - [Decentralized service](#decentralized-service)
    - [Change name of host](#change-name-of-host)
    - [Current scope](#current-scope)
    - [Install and configure postfix](#install-and-configure-postfix)
    - [Install and configure ZeroConf - avahi](#install-and-configure-zeroconf---avahi)
    - [Configure email clients](#configure-email-clients)
    - [Chat system idea](#chat-system-idea)
  - [Decentralized infrastructure](#decentralized-infrastructure)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

# Community-network-email-service

Email services for a LAN or a bigger intranet without using Internet

## Idea

Explore more decentralized ways to run mail services

1. Decentralized service: Each participant have its own SMTP server, then, each participant have its own mailbox in its trusted computer, hence we do not depend on any central service to exchange messages with the other peers.
2. decentralized network infrastructure: The best scenario is probably a wireless mesh network / community (wireless) network. Specially if we compare with a corporate network based on access points

## Decentralized service

### Change name of host

Your current hostname will append .local

It is going to be used to send and receive emails and maybe you want to change it

In case of two hosts with the same name a number will be added at the end ([src1](https://serverfault.com/questions/615561/avahi-how-do-i-setup-multiple-servers-that-originally-have-the-same-hostname/616612#616612) [src2](https://tools.ietf.org/html/rfc6762#section-9))

To change the hostname (from myoldhostname to mynewhostname) use:

    hostnamectl set-hostname mynewhostname.local

you would probably require to change records in `/etc/hosts`, check also matches to replace in postfix directory `grep -ir myoldhostname /etc/postfix/*` alternatively you can specify mynewhostname.local running again postfix configuration `dpkg-reconfigure postfix`

### Current scope

the current scope is *debian distribution in a laptop* other targets are obviously useful but at the moment out of scope (for me)

### Install and configure postfix

    sudo apt install postfix

In the dialogs go to the default ones, when it says your domain, put the hostname (machine's name in the local domain, for example: mymachine.local)

Allow sending to raw IPs (example: user@10.1.1.1) [source](https://serverfault.com/questions/373350/postfix-allow-sending-to-raw-ip-address)

    sudo postconf -p "resolve_numeric_domain = yes"

Use [mDNS](https://en.wikipedia.org/wiki/Multicast_DNS) .local domains (example: user@host1.local) [more info about smtp_host_lookup](http://www.postfix.org/postconf.5.html#smtp_host_lookup)

    sudo postconf -p "smtp_host_lookup = dns, native"

Disable chroot for smtp client in `/etc/postfix/master.cf` is required according to [this source](http://postfix.1071664.n5.nabble.com/postfix-not-resolving-mDNS-lookups-make-it-work-in-a-LAN-without-internet-td102574.html) - or find the config to make it work inside chroot. Disabling chroot has security implications; a host using your smtp server to send messages could make malformed emails to exploit a vulnerability and gain access to your machine, the current postfix configuration allows this behavior when sending email to the current domain ([while everything else is rejected unless it's destined for your box or otherwise explicitly allowed](https://serverfault.com/questions/262100/configure-postfix-to-only-allow-outgoing-mail-from-localhost/262102#262102))

    sudo postconf -F 'smtp/unix/chroot = n'

catch all emails with your user account [source](https://tecadmin.net/setup-catch-all-email-account-in-postfix/)

```sh
sudo tee /etc/postfix/virtual <<END
@${HOSTNAME}.local  ${SUDO_USER:-$USER}
END
sudo postmap /etc/postfix/virtual
sudo postconf -e "virtual_alias_maps = hash:/etc/postfix/virtual"
```

after all these changes restart the service

    sudo systemctl restart postfix

extra note: [same source](http://postfix.1071664.n5.nabble.com/postfix-not-resolving-mDNS-lookups-make-it-work-in-a-LAN-without-internet-td102574.html) shows that postfix read `/etc/hosts` only when restarted

### Install and configure ZeroConf - avahi

[about avahi and zeroconf](https://wiki.debian.org/ZeroConf)

install (read important security note about avahi):

    sudo apt-get install avahi-daemon avahi-discover libnss-mdns

**important security note**: This service does autodiscovery of your computer in the current local network you are (library, airport, your home, public wifi network, etc.), you would want to be found *sometimes* so please take care when to enable it and when not [source](https://wiki.debian.org/ZeroConf#turning_it_off)

start at bootup

    sudo systemctl enable avahi-daemon

prevent from starting at bootup

    sudo systemctl disable avahi-daemon

stop avahi service now

    sudo systemctl stop avahi-daemon.socket
    sudo systemctl stop avahi-daemon.service

start avahi service now

    sudo systemctl start avahi-daemon.socket
    sudo systemctl start avahi-daemon.service

check current status of avahi service

    sudo systemctl status avahi-daemon.socket
    sudo systemctl status avahi-daemon.service

informational note: after installing avahi-daemon, it changes the hosts line in `/etc/nsswitch.conf` to check mDNS hosts and lookups before DNS:

    hosts:          files mdns4_minimal [NOTFOUND=return] dns myhostname

#### discover other hosts

install avahi-browse tool

    apt install avahi-utils

discover other hosts:

    avahi-browse --all --ignore-local --resolve --terminate


### Configure email clients

- [thunderbird](https://www.thunderbird.net): https://askubuntu.com/questions/301988/using-movemail-with-thunderbird-on-ubuntu
  - update 2023-1-30: looks like thunderbird does not expose clearly the movemail thing
- [claws mail](https://www.claws-mail.org/): create account / protocol: local mbox file

### WIP: Chat system idea / IMAP mailbox

this is WIP, do not continue from here because requires extra configuration still not documented

delta.chat transforms email in a chat. Requires a POP/IMAP server.  It is very easy to install dovecot imap for localhost usage

    sudo apt install dovecot-imapd

in `/etc/dovecot/conf.d/10-master.conf` uncomment `port = 143` in `inet_listener imap` section

    sudo service dovecot restart

once dovecot is enabled, the previous configured mailbox will stop working (to make it work again `service dovecot stop` or uninstall dovecot), configure the imap account in your favourite client with no security (143 for imap and 25 for smtp)

---

to install delta.chat in debian 10 it is tricky, you have to download .deb dependency of [libssl1.0.0](https://packages.debian.org/jessie/libssl1.0.0) and install [DeltaChat-ubuntu-18_10-0.104.0.deb](https://github.com/deltachat/deltachat-desktop/releases/download/v0.104.0/DeltaChat-ubuntu-18_10-0.104.0.deb). [19.04 and 19.10 fail this way](https://support.delta.chat/t/delta-chat-desktop-does-not-start-on-debian-10-buster/589)

and at the moment is not working

current followup https://support.delta.chat/t/lan-decentralized-no-internet-setup-mdns-smtp-server-postfix-delta-chat-imap-server-dovecot/583/2

## Decentralized infrastructure

*The glue* to have a name resolution thing that works in layer 3 - [bmx7](https://github.com/bmx-routing/bmx7)-[babel](https://www.irif.fr/~jch/software/babel/)-[olsr](http://www.olsr.org/mediawiki/index.php/Main_Page) (because theoretically, combining mDNS that does discoveries in layer 2 with [batman-advance](https://www.open-mesh.org/projects/batman-adv/wiki) should work)

TODO

ideas:

- https://github.com/mwarning/KadNode/blob/master/misc/manpage.md#name-service-switch-nss
    - https://openwrt.org/packages/pkgdata/kadnode
- https://openwrt.org/docs/guide-developer/mdns
- https://openwrt.org/docs/guide-user/network/zeroconfig/zeroconf
- https://github.com/libremesh/lime-packages/issues/303
